/* 
Activity:
1. In the S19 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
14. Create a git repository named S19.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.
*/

let getCube = 2**3
answer = `the cube of 2 is ${getCube}`
console.log(answer);


/* const homeAddress = (street, state, zip) => {
    return `I live at ${street}, ${state} ${zip} `
}

let printAddress = homeAddress('258 Washington Ave NW', 'California', 90011);
console.log(printAddress); */

homeAddress = ['258 Washington Ave NW', 'California', 90011]
const [street, state, zip] = homeAddress;
console.log(`I live at ${street}, ${state} ${zip}`)




/* const animal = (name_a, type, weight, length) => {
    return `${name_a} is a ${type}, He weighed ${weight} kgs with a measurement of ${length} `
}

let printAnimal = animal('Brownie', 'Shihtzu', 10, '40 inches');
console.log(printAnimal); */
animal = ['brownie', 'shihtzu', 10, '40 inches']
const [name_a, type, weight, length] = animal;
console.log(`${name_a} is a ${type}, He weighed ${weight} kgs with a measurement of ${length} `)



const number_sample = [1, 2, 3, 4, 5]

number_sample.forEach(x => console.log(x));



function reduceNumber(total, num) {
    return total + num
}

console.log(number_sample.reduce(reduceNumber, 0));

class dog{
    constructor(name_d, age, breed){
        this.name_d = name_d
        this.age = age
        this.breed = breed
    }
}

const dog1 = new dog('Frankie', 5, 'Miniature Dachshund')
console.log(dog1)


