/* 

ES6 updates
Arrow functions         
    implicit return statements,

    ECMA Script - standard that is used to create the implementations of the language, one of which is javascript.


*/

// JS ES6 updates

// exponent operator
    // pre es6
    const firstNum = Math.pow(8,2);
    console.log(firstNum); // 64


// es6
const secondNum = 8**2;
console.log(secondNum);



/* 
    Create a name variable that acceps a name of a person 
    create a message, greeting the person and welcoming him/her in the programming field
    login the console messatge
*/

// Template literals (es6) - allows writing of strings without the use of concat operator; helps in the terms of readability of the codes as well as the efficiency of the work.

var name = 'John';
// pre es6
/* let message = 'hello ' + name + " welcome to the programming field"

console.log(message);
 */


//es6
message = `hello ${name} welcome to the programming field`
console.log(message);

// Multiline

const anotherMessage = `
${name} attended a math competition.
He won it by solviong the problem 8**2 with the answer of ${secondNum}
`

console.log(anotherMessage)

// computation inside the template literals

const interestRate = .10;
const principal = 1000;
console.log(`The interest on your savings is ${principal*(interestRate)}`)

// array destructuring - allows upacking elements in arrays into distinct variables; allows naming of array elemetns with variable instead of using index numbers

/* 
create an array that has firstname, middleName and last name elements.

    SYNTAX 
        let/const [variableA, variableB, variableC,.. variableN] = arrayName;
*/
// pre-es6
fullName = ['Juan', 'Dela', 'Cruz'];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// es6
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`hello ${fullName[0]} ${fullName[1]} ${fullName[2]}!`)

/* 
    Create an object that contains a woman's givenName, maidenName, familyName

    log in the console each key (1row - 1key-value pair)
*/

// pre es6
let girl_name = {
    firstName : 'Jane',
    maidenName : 'Smith',
    familyName : 'Jones'
}

console.log(girl_name.familyName);
console.log(girl_name.firstName);
console.log(girl_name.maidenName);

// es6

const{givenName, maidenName, familyName} = girl_name;
console.log(girl_name.familyName);
console.log(girl_name.firstName);

/* 

Create a function that displays logs a person;'s first name, middle inital and lastnamt in the console

*/

/* 

Arrow Function
    Compact alternative syntace to traditional functions;
    Useful for code snippents where creating fuctions will be reused in any other parts of the code;
    Dont repeat yourself - there is no need to create a function that will not be used in the other parts/portions of the code


    parts
        declatration
        function name
        parameters
        statements
        invoke/call tha function

*/

function full_Name(firstName, middleInitial, lastName){
    console.log(firstName);
    console.log(middleInitial);
    console.log(lastName);
    console.log(firstName, middleInitial, lastName);
}

full_Name('Jherson', 'I', "Dignadice");

// es6

// arrow function
const printFName=(fname, mname, lname) => {
    console.log(fname, mname, lname);
}

printFName('Will', 'D', 'Smith')

/* 
    use for reach method to log each student in the console
*/

const students = ['john','jane', 'joe']

/* function listStudents(function(student){
    console.student(student)
}) */

/* students.forEach(
    function(pick){
        console.log(pick);
    }
) */

// If you have 2 or mor eparameters, enclose them inside a pair of parentheses.

students.forEach(x => console.log(x));

/* 
    create a funtion that adds two numbers using return statement
*/

// Arrow Function - implicit return statement - return statements are omitted because even without them, js implictly add them for the result of the function

// Pre es6
function addNumbers(a,b){
    return a + b; // with return
}

let total = addNumbers (1,2);
console.log(total);

// es6

const adds = (x, y) => x + y; // without return
let totals = adds(3,5);
console.log(totals);

// Arrow Function - Default Function Argument Value

const greet = (name_x = 'user') => {
    return `Good morning, ${name_x}`
}

console.log(greet())
// once the furntion has specified parameter value
console.log(greet(`Result of the specified parameter : ${greet('john')}`))


/* 
    create a car object using the object method
     car fields
        name
        brand
        year
*/

/* let car = {
    name_c: 'STI Impreza',
    brand: 'Subaru',
    year: 2008,

} */

/* function car(name_c, brand, year){
    this.name_c = name_c
    this.brand = brand
    this.year = year 
}

const car1 = new car('Honda', 'vios', 2020);
console.log(car1) */
 
//Class constructor

class car{
    // constructor keyword - special method of creating/initializing an object for the "car" class
    constructor(brand, name, year){
        // this - sets the properties that refers to the 'car' class
        this.brand = brand
        this.name = name
        this.year = year
    }
};

const car1 = new car('subaru', 'impreza', 2008);
console.log(car1)
 
// class keyword declares the creation of a 'car' object

const car2 = new car();
console.log(car2);
car2.brand = "Toyota",
car2.name = "Raize",
car2.year = 2020
console.log(car2);


/* 

    Create a variable taht stores a number

    Create an if-else statement using ternary operator

    condition = if the number is <= 0, return true, if it is > 0, return false

*/

let num = 10;

/* if (num <= 0){
    console.log(true)
}else{
    console.log(false)
} */


(num <= 0)? console.log(true) : console.log(false);























